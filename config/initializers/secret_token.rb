# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
FirstApp::Application.config.secret_key_base = 'f1ea33ac4359d840139f3592df986d915494fe5d526741c0f2a9ddd341eefa1719cd841195fd8d6b4e7c47ef41eb3af591fcd8cebced307f3223bd287ea985f0'
